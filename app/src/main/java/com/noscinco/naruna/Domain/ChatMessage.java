package com.noscinco.naruna.Domain;

public class ChatMessage {
    private String msgText;
    private boolean tipo;

    public ChatMessage(String msgt, boolean tipo) {
        this.msgText = msgt;
        this.tipo = tipo;
    }

    public Boolean getTipo() {
        return tipo;
    }

    public String getMsgText() {
        return msgText;
    }
}