package com.noscinco.naruna.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.noscinco.naruna.R;

public class SplashActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private static int SPLASH_TIME_OUT = 3000;
    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.splash_activity);

        progressBar = (ProgressBar) findViewById(R.id.progressBarSplash);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
    private void exibirProgressBar(boolean exibir){
        progressBar.setVisibility(exibir ? View.VISIBLE : View.GONE);
    }

    class TaskSplash extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            exibirProgressBar(true);
        }
        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }


    }
}
