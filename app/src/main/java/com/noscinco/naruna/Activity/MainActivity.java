package com.noscinco.naruna.Activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import com.noscinco.naruna.Adapter.AdapterChat;
import com.noscinco.naruna.Domain.ChatMessage;
import com.noscinco.naruna.R;

import java.util.ArrayList;
import java.util.List;

import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.GsonFactory;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.ui.AIDialog;

public class MainActivity extends AppCompatActivity implements AIDialog.AIDialogListener{
    private RecyclerView recyclerViewChat;
    final private List<ChatMessage> mensagens = new ArrayList<ChatMessage>();
    private String respostaNaruna;
    private EditText mensagemUser;
    private ImageView sendMsg;
    private DatabaseReference reference;
    private Boolean flagFab = true;
    private AdapterChat adapter;
    private AIDialog aiDialog;

    private static final String TAG = MainActivity.class.getName();
    private Gson gson = GsonFactory.getGson();
    private AIDataService aiDataService;

    private AlertDialog alertDialog;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mensagemUser = (EditText) findViewById(R.id.etChat);
        sendMsg = (ImageView) findViewById(R.id.sendMensage);
        sendMsg.setOnClickListener(enviarMensagem());

        recyclerViewChat = (RecyclerView) findViewById(R.id.recyclerChat);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerViewChat.setLayoutManager(linearLayoutManager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Naruna");


        if(verificaConexao())
            mensagens.add(new ChatMessage("Ola, eu sou Naruna<3",true));

        adapter = new AdapterChat(mensagens,this);
        recyclerViewChat.setAdapter(adapter);

        final AIConfiguration config = new AIConfiguration("f8f4afcaf58c4f8a872b89beb6c67945",
                AIConfiguration.SupportedLanguages.PortugueseBrazil,
                AIConfiguration.RecognitionEngine.System);
        aiDataService = new AIDataService(config);

        mensagemUser.setOnFocusChangeListener(clicouEditText());

    }

    private View.OnFocusChangeListener clicouEditText() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                recyclerViewChat.smoothScrollToPosition( mensagens.size());

            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_about:
                Toast.makeText(getBaseContext(),"clicou em sobre",Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("INFORMAÇÕES");
                builder.setMessage("ChatBot Naruna versão 1.0\n" +
                        "Desenvolvedores:\n" +
                        "Anna Thais\n" +
                        "Luis Carlos\n" +
                        "Marcelino Sena\n" +
                        "Pedro Goes\n" +
                        "Thuan Matheus\n");
                alertDialog = builder.create();
                alertDialog.show();

                break;
            case R.id.action_settings:
                Toast.makeText(getBaseContext(),"clicou em configurações",Toast.LENGTH_SHORT).show();

                break;
        }
        return true;
    }

    private View.OnClickListener enviarMensagem() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(verificaConexao()) {
                    String novaMsg = mensagemUser.getText().toString().trim();
                    if (!TextUtils.isEmpty(novaMsg)) {
                        ChatMessage msg = new ChatMessage(novaMsg, false);//usuario
                        mensagens.add(msg);
                        int novaPosicao = mensagens.size();
                        adapter.notifyDataSetChanged();
                        adapter.notifyItemInserted(novaPosicao);

                        recyclerViewChat.scrollToPosition(novaPosicao);
                        sendRequest();
                        Log.i("LISTA", adapter.getList());
                        mensagemUser.setText("");
                    }
                }else {
                    alertaConexao();
                    mensagemUser.setText("");
                }
            }
        };
    }



    private void sendRequest(){
        if(!mensagemUser.equals("")){
            final String queryString = String.valueOf(mensagemUser.getText());

            final AsyncTask<String,Void,AIResponse> task= new AsyncTask<String,Void,AIResponse>(){
                private AIError aiError;
                @Override
                protected AIResponse doInBackground(final String... params){
                    final AIRequest request = new AIRequest();
                    String query = params[0];
                    if (!TextUtils.isEmpty(query))
                        request.setQuery(query);

                    try {
                        return aiDataService.request(request);
                    } catch (final AIServiceException e) {
                        aiError = new AIError(e);
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(final AIResponse response) {
                    if (response != null) {
                        onResult(response);
                    } else {
                        onError(aiError);
                    }
                }
            };
            task.execute(queryString);
        }

    }
    public void onResult(final AIResponse response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Result result = response.getResult();
                respostaNaruna = result.getFulfillment().getSpeech();
                ChatMessage msg = new ChatMessage( respostaNaruna,true);
                mensagens.add(msg);
                int novaPosicao = mensagens.size()-1;
                adapter.notifyDataSetChanged();
                adapter.notifyItemInserted(novaPosicao);
                recyclerViewChat.scrollToPosition(novaPosicao);

                Log.i("LISTA",adapter.getList());
                Log.i(TAG, "Speech: " + respostaNaruna);


                final Metadata metadata = result.getMetadata();
                if (metadata != null) {
                    Log.i(TAG, "Intent id: " + metadata.getIntentId());
                    Log.i(TAG, "Intent name: " + metadata.getIntentName());
                }


            }

        });
    }

    @Override
    public void onError(AIError error) {

    }

    @Override
    public void onCancelled() {

    }
    public  boolean verificaConexao() {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;
        } else {
            conectado = false;
        }
        return conectado;
    }
    private void alertaConexao() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Algo deu errado");
        builder.setMessage("Parece que você está sem conexão com a internet");
        alertDialog = builder.create();
        alertDialog.show();
    }


    public void showSoftKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);

        }
    }
}
