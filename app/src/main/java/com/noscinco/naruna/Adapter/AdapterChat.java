package com.noscinco.naruna.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noscinco.naruna.Domain.ChatMessage;
import com.noscinco.naruna.R;

import java.util.List;

public class AdapterChat extends RecyclerView.Adapter{

    private List<ChatMessage> mensagens;
    private Context context;
    public AdapterChat(List<ChatMessage> mensagens,Context context){
        this.mensagens = mensagens;
        this.context = context;
    }



    public void inserirMensagem(ChatMessage chat){
        mensagens.add(chat);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(context).inflate(R.layout.mensagem_chat,parent,false);
        ChatViewHolder holder = new ChatViewHolder(view);
        return holder;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder,
                                 int position) {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.topMargin = 40;
        ChatViewHolder holder = (ChatViewHolder) viewHolder;
        ChatMessage chat = mensagens.get(position);
        if(chat.getTipo()){
            params.gravity = Gravity.LEFT;
            params.rightMargin = 50;
            holder.msg.setLayoutParams(params);
            holder.msg.setText(chat.getMsgText());
            holder.msg.setBackgroundResource(R.drawable.rounded_rectangle_vermelho);


        }else{
            params.gravity = Gravity.RIGHT;
            params.leftMargin = 50;
            holder.msg.setLayoutParams(params);
            holder.msg.setText(chat.getMsgText());
            holder.msg.setBackgroundResource(R.drawable.rounded_rectangle_azul);
        }


    }
    public String getList(){
        String lista = "";
        for(ChatMessage c:mensagens){
            lista+=c.getMsgText();
        }
        return lista;
    }


    @Override
    public int getItemCount() {
        return mensagens.size();
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder{
        final TextView msg;
        final LinearLayout layoutMsg;
        public ChatViewHolder(View view){
            super(view);
            msg = (TextView) view.findViewById(R.id.msgUser);
            layoutMsg = (LinearLayout) view.findViewById(R.id.layout_mensagens);

        }
    }
}